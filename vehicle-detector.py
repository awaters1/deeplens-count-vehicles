# Parts of this code taken from Amazon's examples - https://docs.aws.amazon.com/deeplens/latest/dg/deeplens-inference-lambda-create.html

# Continually monitors the camera for vehicles

from threading import Thread, Event, Lock
import os
import json
import numpy as np
import awscam
import cv2
import greengrasssdk
from datetime import datetime
import traceback
import time
import boto3

# The name bucket
vehicle_detection_bucket = os.environ['VEHICLE_DETECTION_BUCKET']

class ImageUpload(Thread):

    def __init__(self):
        super(ImageUpload, self).__init__()
        self.detections_to_upload = []
        self.stop_request = Event()
        self.process_work = Event()
        self.frames_lock = Lock()

    def get_base_key(self, now):
        year = now.strftime('%Y')
        month = now.strftime('%m')
        day = now.strftime('%d')
        hour = now.strftime('%H')
        minute = now.strftime('%M')
        key = '%s/%s/%s/%s/%s' % \
              (year, month, day, hour, minute)
        return key

    def resize_to_jpeg(self, frame):
        frame_resized = cv2.resize(frame, (858, 480))
        ret, jpeg = cv2.imencode('.jpg', frame_resized)
        return jpeg

    def process_detections(self, detections):
        s3 = boto3.resource('s3')
        bucket = s3.Bucket(vehicle_detection_bucket)
        for detection in detections:
            frame, labeled_frame, inference_results, bounding_boxes = detection
            now = datetime.utcnow()
            base_key = '%s/%s' % ('detection', self.get_base_key(now))
            filename = now.strftime('%Y-%m-%d_%H-%M-%S-%f')

            # Upload frame
            jpeg = self.resize_to_jpeg(frame)
            key = '%s/%s-frame.jpg' % (base_key, filename)
            bucket.put_object(Key=key, Body=jpeg.tobytes(), ContentType='image/jpeg')
            # Upload labels
            jpeg = self.resize_to_jpeg(labeled_frame)
            key = '%s/%s-labeled.jpg' % (base_key, filename)
            bucket.put_object(Key=key, Body=jpeg.tobytes(), ContentType='image/jpeg')
            # Upload inference_results
            key = '%s/%s-inference-results.json' % (base_key, filename)
            bucket.put_object(Key=key, Body=json.dumps(inference_results), ContentType='application/json')
            # Upload bounding boxes
            key = '%s/%s-bounding_boxes.json' % (base_key, filename)
            bucket.put_object(Key=key, Body=json.dumps(bounding_boxes), ContentType='application/json')

            print 'Uploaded %s' % (key)

    def run(self):
        while not self.stop_request.isSet():
            self.process_work.wait()
            self.frames_lock.acquire()
            my_detections = self.detections_to_upload[:]
            self.detections_to_upload = []
            self.frames_lock.release()
            self.process_work.clear()

            self.process_detections(my_detections)

    def upload_detection(self, frame, labeled_frame, inference_results, bounding_boxes):
        self.frames_lock.acquire()
        self.detections_to_upload.append((frame, labeled_frame, inference_results, bounding_boxes))
        self.frames_lock.release()
        self.process_work.set()

    def join(self):
        self.stop_request.set()


class LocalDisplay(Thread):
    """ Class for facilitating the local display of inference results
        (as images). The class is designed to run on its own thread. In
        particular the class dumps the inference results into a FIFO
        located in the tmp directory (which lambda has access to). The
        results can be rendered using mplayer by typing:
        mplayer -demuxer lavf -lavfdopts format=mjpeg:probesize=32 /tmp/results.mjpeg
    """
    def __init__(self, resolution):
        """ resolution - Desired resolution of the project stream """
        # Initialize the base class, so that the object can run on its own
        # thread.
        super(LocalDisplay, self).__init__()
        # List of valid resolutions
        RESOLUTION = {'1080p' : (1920, 1080), '720p' : (1280, 720), '480p' : (858, 480)}
        if resolution not in RESOLUTION:
            raise Exception("Invalid resolution")
        self.resolution = RESOLUTION[resolution]
        # Initialize the default image to be a white canvas. Clients
        # will update the image when ready.
        self.frame = cv2.imencode('.jpg', 255*np.ones([640, 480, 3]))[1]
        self.stop_request = Event()

    def run(self):
        """ Overridden method that continually dumps images to the desired
            FIFO file.
        """
        # Path to the FIFO file. The lambda only has permissions to the tmp
        # directory. Pointing to a FIFO file in another directory
        # will cause the lambda to crash.
        result_path = '/tmp/results.mjpeg'
        # Create the FIFO file if it doesn't exist.
        if not os.path.exists(result_path):
            os.mkfifo(result_path)
        # This call will block until a consumer is available
        with open(result_path, 'w') as fifo_file:
            while not self.stop_request.isSet():
                try:
                    # Write the data to the FIFO file. This call will block
                    # meaning the code will come to a halt here until a consumer
                    # is available.
                    fifo_file.write(self.frame.tobytes())
                except IOError:
                    continue

    def set_frame_data(self, frame):
        """ Method updates the image data. This currently encodes the
            numpy array to jpg but can be modified to support other encodings.
            frame - Numpy array containing the image data tof the next frame
                    in the project stream.
        """
        ret, jpeg = cv2.imencode('.jpg', cv2.resize(frame, self.resolution))
        if not ret:
            raise Exception('Failed to set frame data')
        self.frame = jpeg

    def join(self):
        self.stop_request.set()

def greengrass_infinite_infer_run():
    """ Entry point of the lambda function"""
    try:
        # This vehicle detector is implemented as single shot detector (ssd).
        model_type = 'ssd'
        output_map = {
            1: 'car left',
            2: 'car right',
            3: 'suv left',
            4: 'suv right',
            5: 'truck left',
            6: 'truck right',
            7: 'van left',
            8: 'van right',
            9: 'motorcycle left',
            10: 'motorcycle right',
            11: 'person'
        }
        # Create an IoT client for sending to messages to the cloud.
        client = greengrasssdk.client('iot-data')
        iot_topic = '$aws/things/{}/infer'.format(os.environ['AWS_IOT_THING_NAME'])
        # Create a local display instance that will dump the image bytes to a FIFO
        # file that the image can be rendered locally.
        local_display = LocalDisplay('480p')
        local_display.start()

        image_upload = ImageUpload()
        image_upload.start()

        # Set the threshold for detection
        detection_threshold = 0.75
        # The height and width of the training set images
        input_height = 240
        input_width = 429

        print('Loading TF model')
        model_path = '/opt/awscam/artifacts/mobilenet/frozen_inference_graph.xml'
        model = awscam.Model(model_path, {'GPU': 1})
        print('done loading model')

        last_date = time.time()
        frames = 0

        # Do inference until the lambda is killed.
        while True:
            # Get a frame from the video stream
            ret, frame = awscam.getLastFrame()
            if not ret:
                raise Exception('Failed to get frame from the stream')

            original_frame = frame.copy()

            # Resize frame to the same size as the training set.
            frame_resize = cv2.resize(frame, (input_width, input_height))
            # Run the images through the inference engine and parse the results using
            # the parser API, note it is possible to get the output of doInference
            # and do the parsing manually, but since it is a ssd model,
            # a simple API is provided.
            parsed_inference_results = model.parseResult(model_type,
                                                         model.doInference(frame_resize))

            # print(parsed_inference_results)
            # Compute the scale in order to draw bounding boxes on the full resolution
            # image.
            yscale = float(frame.shape[0]/ float(input_height))
            xscale = float(frame.shape[1]/ float(input_width))
            # Get the detected faces and probabilities
            bounding_boxes = []
            for obj in parsed_inference_results[model_type]:
                if obj['prob'] > detection_threshold:
                    # Add bounding boxes to full resolution frame
                    xmin = int(xscale * obj['xmin'])
                    ymin = int(yscale * obj['ymin'])
                    xmax = int(xscale * obj['xmax'])
                    ymax = int(yscale * obj['ymax'])
                    # See https://docs.opencv.org/3.4.1/d6/d6e/group__imgproc__draw.html
                    # for more information about the cv2.rectangle method.
                    # Method signature: image, point1, point2, color, and tickness.
                    cv2.rectangle(frame, (xmin, ymin), (xmax, ymax), (255, 165, 20), 10)
                    # Amount to offset the label/probability text above the bounding box.
                    text_offset = 15
                    # See https://docs.opencv.org/3.4.1/d6/d6e/group__imgproc__draw.html
                    # for more information about the cv2.putText method.
                    # Method signature: image, text, origin, font face, font scale, color,
                    # and tickness
                    obj_label = output_map[obj['label']]
                    cv2.putText(frame, '{} {:.2f}%'.format(obj_label, obj['prob'] * 100),
                                (xmin, ymin-text_offset),
                                cv2.FONT_HERSHEY_SIMPLEX, 2.5, (255, 165, 20), 6)
                    # Store label and probability to send to cloud
                    print('%s - %.2f%%' % (obj_label, obj['prob'] * 100))
                    bounding_boxes.append({
                        'xmin': xmin,
                        'ymin': ymin,
                        'xmax': xmax,
                        'ymax': ymax,
                        'label': obj_label,
                        'probability': obj['prob']
                    })

            if len(bounding_boxes) > 0:
                image_upload.upload_detection(original_frame, frame, parsed_inference_results, bounding_boxes)

            # Set the next frame in the local display stream.
            local_display.set_frame_data(frame)

            frames = frames + 1
            time_diff = time.time() - last_date
            if time_diff >= 5:
                print('FPS: %f' % (frames / time_diff))
                last_date = time.time()
                frames = 0

    except Exception as ex:
        print(traceback.format_exc())

greengrass_infinite_infer_run()

# This is a dummy handler and will not be invoked
# Instead the code above will be executed in an infinite loop for our example
def function_handler(event, context):
    return
