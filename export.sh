#!/bin/bash

# Copy this to models/research/
# Run with TF 1.9 for the intel model optimizer
# in the intel model optimizer pipenv

INPUT_TYPE=image_tensor
PIPELINE_CONFIG_PATH=/mnt/storage/development/count-cars/models/model/pipeline.config
TRAINED_CKPT_PREFIX=/mnt/storage/development/count-cars/models/model/model.ckpt-27960
EXPORT_DIR=/mnt/storage/development/count-cars/models/export-1.9/
python object_detection/export_inference_graph.py \
    --input_type=${INPUT_TYPE} \
    --pipeline_config_path=${PIPELINE_CONFIG_PATH} \
    --trained_checkpoint_prefix=${TRAINED_CKPT_PREFIX} \
    --output_directory=${EXPORT_DIR}
