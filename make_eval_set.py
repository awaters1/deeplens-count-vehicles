import os
import shutil
import numpy as np

# Take random files from basedir and move them into outdir
basedir = '/mnt/storage/development/count-cars/train-labels'
files = os.listdir(basedir)
outdir = '/mnt/storage/development/count-cars/eval-labels'
random_indexes = np.random.permutation(len(files))
# Take 10% of our data
for i in range(int(len(files) * 0.10)):
    pos = random_indexes[i]
    to_move = os.path.join(basedir, files[pos])
    shutil.move(to_move, outdir)
