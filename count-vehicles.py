# Count the vehicles driving by

import os
import json
import cv2
import re
import math
import numpy as np
from PIL import Image
from datetime import datetime
from scipy.spatial import distance
import imagehash

detection_percentage = .90
input_height = 240
input_width = 429
model_type = 'ssd'
output_map = {
    1: 'car left',
    2: 'car right',
    3: 'suv left',
    4: 'suv right',
    5: 'truck left',
    6: 'truck right',
    7: 'van left',
    8: 'van right',
    9: 'motorcycle left',
    10: 'motorcycle right',
    11: 'person no_dir'
}


# key will be time from epoch, value will be an array of vehicle
# hashes at that second, along with what type of vehicle
vehicle_dict = {}

orb = cv2.ORB_create()

# Compute hash and organize into a dictionary
def handle_results(inference_path):
    match = re.search('(\d{4}-\d{2}-\d{2}_\d{2}-\d{2}-\d{2}-\d{6})', inference_path)
    inference_date = datetime.strptime(match.group(0), '%Y-%m-%d_%H-%M-%S-%f')
    print('Analyzing %s from %s' % (inference_path, str(inference_date)))
    with open(inference_path) as fp:
        inference_data = json.load(fp)
    frame_path = inference_path.replace('inference-results', 'frame').replace('json', 'jpg')
    # print('Loading frame %s' % frame_path)
    source_img = cv2.imread(frame_path, 1)

    yscale = float(source_img.shape[0] / float(input_height))
    xscale = float(source_img.shape[1] / float(input_width))

    results = inference_data[model_type]
    for result in results:
        if result['prob'] > detection_percentage:
            xmin = int(xscale * result['xmin'])
            ymin = int(yscale * result['ymin'])
            xmax = int(xscale * result['xmax'])
            ymax = int(yscale * result['ymax'])

            vehicle_type = output_map[result['label']]
            print('Found a %s with %.2f%%' % (vehicle_type, result['prob'] * 100))

            # Crop out the image
            crop = source_img[ymin:ymax, xmin:xmax]

            # compute the descriptors with ORB
            kp, des = orb.detectAndCompute(crop, None)

            # image = cv2.cvtColor(crop, cv2.COLOR_RGB2GRAY)
            image = crop
            # image_hash = dhash(image)
            image_hash = imagehash.dhash(Image.fromarray(crop))

            # cv2.imwrite('output/output-%s-%s-%.2f.jpg' % (inference_date.strftime('%Y-%m-%d_%H-%M-%S-%f'), vehicle_type,
            #                                                result['prob'] * 100), image)

            seconds_offset = (inference_date - datetime(1970, 1, 1)).total_seconds()
            key = math.floor(seconds_offset)
            if key not in vehicle_dict:
                vehicle_dict[key] = []

            vehicle_dict[key].append({
                "image_hash": image_hash,
                "vehicle_type": vehicle_type,
                "file_name": inference_path,
                "direction": vehicle_type.split(' ')[1],
                "type": vehicle_type.split(' ')[0],
                'already_matched': False,
                # "descriptor": des,
                # "kp": kp,
            })


def traverse_directory(path):
    entries = os.listdir(path)
    for entry in entries:
        if os.path.isdir(os.path.join(path, entry)):
            traverse_directory(os.path.join(path, entry))
        elif 'inference-results' in entry:
            handle_results(os.path.join(path, entry))


def compare_hashes(hash1, hash2):
    return distance.hamming(hash1, hash2)


def count_vehicle():
    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
    search_window_seconds = 60
    all_times = vehicle_dict.keys()
    all_times.sort()
    sums = {
        'car': 0,
        'suv': 0,
        'truck': 0,
        'van': 0,
        'motorcycle': 0,
        'person': 0
    }
    for i, time in enumerate(all_times):
        # print('Looking at index %d with time %d' % (i, time))
        prev_time = time
        prev_index = i
        while(prev_index > 0 and time - all_times[prev_index - 1] < search_window_seconds):
            prev_index = prev_index - 1
            prev_time = all_times[prev_index]
        next_time = time
        next_index = i
        while (next_index < len(all_times) - 1 and all_times[next_index + 1] - time < search_window_seconds):
            next_time = all_times[next_index]
            next_index = next_index + 1

        print('Prev Time: %d Current Time: %d Next Time: %d (%d to %d)' % (prev_time, time, next_time, prev_index, next_index))

        for vehicle in vehicle_dict[all_times[i]]:
            if not vehicle['already_matched']:
                sums[vehicle['type']] = sums[vehicle['type']] + 1
            for j in range(prev_index, next_index + 1):
                if j == i:
                    continue
                for other_vehicle in vehicle_dict[all_times[j]]:
                    # Feature descriptors don't really work well
                    # des1 = vehicle['descriptor']
                    # des2 = vehicle['descriptor']
                    # matches = bf.match(des1, des2)
                    # # Sort them in the order of their distance.
                    # matches = sorted(matches, key=lambda x: x.distance)
                    # distances = list(map(lambda x: x.distance, matches))
                    # if len(matches) > 0:
                    #     print(np.sum(distances[:10]))
                    #     print("%s and %s are the same" % (vehicle["file_name"], other_vehicle["file_name"]))

                    # Hashes don't really work out well
                    # same = compare_hashes(vehicle['image_hash'], other_vehicle['image_hash'])
                    same = vehicle['image_hash'] - other_vehicle['image_hash']
                    match_direction = vehicle['direction'] == other_vehicle['direction']
                    match_type = vehicle['type'] == other_vehicle['type']
                    if same > 20 and (match_direction or match_type):
                        print("%s and %s are the same %f match type %s match direction %s" %
                              (vehicle["file_name"], other_vehicle["file_name"], same, match_type, match_direction))
                        other_vehicle['already_matched'] = True
    print(sums)


if __name__ == '__main__':
    # traverse_directory('./samples/')
    traverse_directory('/mnt/storage/development/count-cars/detection/2018/10/14/')
    # print(vehicle_dict)
    count_vehicle()


