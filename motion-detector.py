# Parts of this code taken from PyImageSearch - https://www.pyimagesearch.com/2015/06/01/home-surveillance-and-motion-detection-with-the-raspberry-pi-python-and-opencv/
# Parts of this code taken from Amazon's examples - https://docs.aws.amazon.com/deeplens/latest/dg/deeplens-inference-lambda-create.html

# Continually monitors the camera for motion and when motion is detected it uploads the captured image to S3

from threading import Thread, Event, Lock
import os
import numpy as np
import awscam
import cv2
import greengrasssdk
from datetime import datetime
import traceback
import time
import boto3

# The name bucket
motion_detection_bucket = os.environ['MOTION_DETECTION_BUCKET']

RESOLUTION = {'1080p': (1920, 1080), '720p': (1280, 720), '480p': (858, 480)}

# Background thread to upload images so that the network IO doesn't delay the image processing
class ImageUpload(Thread):

    def __init__(self):
        super(ImageUpload, self).__init__()
        self.frames_to_upload = []
        self.stop_request = Event()
        self.process_work = Event()
        self.frames_lock = Lock()

    def get_base_key(self, now):
        year = now.strftime('%Y')
        month = now.strftime('%m')
        day = now.strftime('%d')
        hour = now.strftime('%H')
        minute = now.strftime('%M')
        key = '%s/%s/%s/%s/%s' % \
              (year, month, day, hour, minute)
        return key

    def resize_to_jpeg(self, frame):
        frame_resized = cv2.resize(frame, (858, 480))
        ret, jpeg = cv2.imencode('.jpg', frame_resized)
        return jpeg

    def process_motion_frames(self, my_frames):
        for frame in my_frames:
            now = datetime.utcnow()
            jpeg = self.resize_to_jpeg(frame)
            base_key = self.get_base_key(now)
            filename = now.strftime('%Y-%m-%d_%H-%M-%S-%f')
            key = 'motion/%s/%s.jpg' % \
                  (base_key, filename)
            s3 = boto3.resource('s3')
            bucket = s3.Bucket(motion_detection_bucket)
            bucket.put_object(Key=key, Body=jpeg.tobytes(), ContentType='image/jpeg')
            print('Uploaded %s' % (key))

    def run(self):
        while not self.stop_request.isSet():
            self.process_work.wait()
            self.frames_lock.acquire()
            my_frames = self.frames_to_upload[:]
            self.frames_to_upload = []
            self.frames_lock.release()
            self.process_work.clear()

            self.process_motion_frames(my_frames)

    def upload_motion_frame(self, frame):
        self.frames_lock.acquire()
        self.frames_to_upload.append(frame)
        self.frames_lock.release()
        self.process_work.set()

    def join(self):
        self.stop_request.set()

# Background thread to create the output stream for viewing
class LocalDisplay(Thread):
    """ Class for facilitating the local display of inference results
        (as images). The class is designed to run on its own thread. In
        particular the class dumps the inference results into a FIFO
        located in the tmp directory (which lambda has access to). The
        results can be rendered using mplayer by typing:
        mplayer -demuxer lavf -lavfdopts format=mjpeg:probesize=32 /tmp/results.mjpeg
    """
    def __init__(self, resolution):
        """ resolution - Desired resolution of the project stream """
        # Initialize the base class, so that the object can run on its own
        # thread.
        super(LocalDisplay, self).__init__()
        # List of valid resolutions
        if resolution not in RESOLUTION:
            raise Exception("Invalid resolution")
        self.resolution = RESOLUTION[resolution]
        # Initialize the default image to be a white canvas. Clients
        # will update the image when ready.
        self.frame = cv2.imencode('.jpg', 255*np.ones([640, 480, 3]))[1]
        self.stop_request = Event()

    def run(self):
        """ Overridden method that continually dumps images to the desired
            FIFO file.
        """
        # Path to the FIFO file. The lambda only has permissions to the tmp
        # directory. Pointing to a FIFO file in another directory
        # will cause the lambda to crash.
        result_path = '/tmp/results.mjpeg'
        # Create the FIFO file if it doesn't exist.
        if not os.path.exists(result_path):
            os.mkfifo(result_path)
        # This call will block until a consumer is available
        with open(result_path, 'w') as fifo_file:
            while not self.stop_request.isSet():
                try:
                    # Write the data to the FIFO file. This call will block
                    # meaning the code will come to a halt here until a consumer
                    # is available.
                    fifo_file.write(self.frame.tobytes())
                except IOError:
                    continue

    def set_frame_data(self, frame):
        """ Method updates the image data. This currently encodes the
            numpy array to jpg but can be modified to support other encodings.
            frame - Numpy array containing the image data tof the next frame
                    in the project stream.
        """
        ret, jpeg = cv2.imencode('.jpg', cv2.resize(frame, self.resolution))
        if not ret:
            raise Exception('Failed to set frame data')
        self.frame = jpeg

    def join(self):
        self.stop_request.set()

# Code to detect when there is motion in a scene
avg_frame = None
def detect_motion(frame):
    global avg_frame

    frame = cv2.resize(frame, RESOLUTION['480p'])

    gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    gray_blur = cv2.GaussianBlur(gray_frame, (21, 21), 0)

    # if the average frame is None, initialize it
    if avg_frame is None:
        avg_frame = gray_blur.copy().astype("float")
        return False, frame
    # accumulate the weighted average between the current frame and
    # previous frames, then compute the difference between the current
    # frame and running average
    cv2.accumulateWeighted(gray_blur, avg_frame, 0.5)
    frame_delta = cv2.absdiff(gray_blur, cv2.convertScaleAbs(avg_frame))

    had_motion = False
    threshold = cv2.threshold(frame_delta, 25, 255, cv2.THRESH_BINARY)[1]

    threshold = cv2.dilate(threshold, None, iterations=2)
    contours, hierarchy = cv2.findContours(threshold.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    for contour in contours:
        (x, y, w, h) = cv2.boundingRect(contour)
        if w > 25 and h > 10:
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
            had_motion = True

    return had_motion, frame

def greengrass_infinite_infer_run():
    """ Entry point of the lambda function"""
    print('-----')
    print('Storing images in %s' % motion_detection_bucket)
    try:
        # Create an IoT client for sending to messages to the cloud.
        client = greengrasssdk.client('iot-data')
        iot_topic = '$aws/things/{}/infer'.format(os.environ['AWS_IOT_THING_NAME'])
        # Create a local display instance that will dump the image bytes to a FIFO
        # file that the image can be rendered locally.
        local_display = LocalDisplay('480p')
        local_display.start()

        image_upload = ImageUpload()
        image_upload.start()

        last_date = time.time()
        frames = 0

        # Do inference until the lambda is killed.
        while True:
            # Get a frame from the video stream
            ret, frame = awscam.getLastFrame()
            if not ret:
                raise Exception('Failed to get frame from the stream')
            had_motion, motion_frame = detect_motion(frame)
            if had_motion:
                image_upload.upload_motion_frame(frame)
            local_display.set_frame_data(motion_frame)

            frames = frames + 1
            time_diff = time.time() - last_date
            if time_diff >= 5:
                print('FPS: %f' % (frames / time_diff))
                last_date = time.time()
                frames = 0

            # Set the next frame in the local display stream.

    except Exception as ex:
        print(traceback.format_exc())
        client.publish(topic=iot_topic, payload='Error in motion detection lambda: {}'.format(ex))

greengrass_infinite_infer_run()

# This is a dummy handler and will not be invoked
# Instead the code above will be executed in an infinite loop for our example
def function_handler(event, context):
    return
