# deeplens-count-vehicles

Repository for hosting the code associated with the DeepLens Count projects.
 
motion-detector.py - Lambda code to run motion detection on vehicle driving by on the road

Some of the files need to be copied into the 
[tensor/flow](https://github.com/tensorflow/models) models repository checkout.